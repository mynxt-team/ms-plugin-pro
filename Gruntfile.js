module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        separator: ';\n',
        stripBanners: {
          block: true,
          line: true
        }
      },
      dist: {
        // the files to concatenate
        src: [
          'app/bower_components/jquery/dist/jquery.min.js',
          'app/bower_components/datatables/media/js/jquery.dataTables.min.js',
          'app/includes/dataTables.bootstrap.js',
          'app/bower_components/bootstrap/dist/js/bootstrap.min.js',
          'app/bower_components/angular/angular.min.js',
          'app/bower_components/angular-datatables/dist/angular-datatables.min.js',
          'app/bower_components/angular-route/angular-route.min.js',
          'app/bower_components/angular-strap/dist/angular-strap.min.js',
          'app/bower_components/angular-strap/dist/angular-strap.tpl.min.js',
          'app/bower_components/ladda/dist/spin.min.js',
          'app/bower_components/ladda/dist/ladda.min.js',
          'app/bower_components/angular-ladda/dist/angular-ladda.min.js',
          'app/bower_components/angular-loading-bar/build/loading-bar.min.js',
          'app/bower_components/iframe-resizer/js/iframeResizer.contentWindow.min.js',
          'app/app.js',
          'app/components/**/*.js'
        ],
        // the location of the resulting JS file
        dest: 'app/dist/<%= pkg.name %>.js'
      }
    },
    concat_css: {
      options: {
        // Task-specific options go here.
      },
      all: {
        src: [
          'app/bower_components/bootstrap/dist/css/bootstrap.min.css',
          'app/bower_components/ladda/dist/ladda-themeless.min.css',
          'app/bower_components/angular-loading-bar/build/loading-bar.min.css',
          'app/app.css'
        ],
        dest: "app/dist/<%= pkg.name %>.css"
      }
    },
    uglify: {
      options: {
        // the banner is inserted at the top of the output
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
        mangle: false
      },
      dist: {
        files: {
          'app/dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-concat-css');

  grunt.registerTask('default', ['concat', 'uglify', 'concat_css']);
};