'use strict';

angular.module('mynxt', [])
  .service('MyNXT', ['$http', function ($http) {
    var env = 'PROD';
    var walletUrl = 'https://wallet.mynxt.info';
    var apiPath = '/api/0.1/';
    var blockExplorerUrl = 'https://mynxt.info/api/0.1/public/index.php/';

    if(window.location.hostname === 'walletdev.mynxt.info') {
      walletUrl = 'https://walletdev.mynxt.info';
    }

    this.nrsRequest = function (requestType, data) {
      return $http({
        method: 'POST',
        url: walletUrl + '/nxt?requestType=' + requestType,
        params: data
      });
    };

    this.getAccounts = function () {
      return $http.get(walletUrl + apiPath + 'user/account');
    };

    this.queryExplorer = function (requestType, data) {
      if(!requestType) return;

      if(!data) data = {};

      return $http({
        method: 'GET',
        url: blockExplorerUrl + requestType,
        params: data
      });
    };

    this.sendTransaction = function (data, callback) {
      if(env !== 'PROD') return;

      var messageId = Math.floor(Math.random() * 10000000);

      data.messageId = messageId;

      parent.postMessage(data, walletUrl);

      function listener (event) {
        if(event.origin !== walletUrl) return;

        if(event.data && event.data.messageId == messageId) {
          delete event.data.messageId;
          callback(event.data);
        }

        window.removeEventListener('message', listener, false);
      }

      window.addEventListener('message', listener, false);
    };

  }]);