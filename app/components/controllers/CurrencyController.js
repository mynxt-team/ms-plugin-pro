'use strict';

angular.module('ms-plugin')
  .controller('CurrencyController', function ($scope, $rootScope, MyNXT, $filter) {
    var currency = $scope.$parent.currency;

    $scope.currencyBalance = 0;

    $scope.isReservable = function () {
      for(var i = 0; i < currency.types.length; i++) {
        if(currency.types[i] == "RESERVABLE" && currency.issuanceHeight > $rootScope.blockHeight) return true;
      }
      return false;
    };

    $scope.isClaimable = function () {
      for(var i = 0; i < currency.types.length; i++) {
        if(currency.types[i] == "CLAIMABLE" && currency.issuanceHeight <= $rootScope.blockHeight) return true;
      }
      return false;
    };

    $scope.isExchangeable = function () {
      for(var i = 0; i < currency.types.length; i++) {
        if(currency.types[i] == "EXCHANGEABLE" && currency.issuanceHeight < $rootScope.blockHeight) return true;
      }
      return false;
    };

    $scope.step = 1 / Math.pow(10, currency.decimals);

    $scope.buy = {
      offers: [],
      price: null,
      transaction: null
    };

    $scope.sell = {
      offers: [],
      price: null,
      transaction: null
    };

    $scope.transfer = {
      quantity: 0,
      transaction: null
    };

    $scope.reserve = {
      transaction: null
    };

    $scope.claim = {
      transaction: null
    };


    $scope.reserveCurrency = function () {
      $scope.loading = true;

      var amountPerUnit =  $filter('times')($scope.reserve.amountPerUnit, Math.pow(10, 8));
      amountPerUnit = $filter('divide')(amountPerUnit,  Math.pow(10, currency.decimals));

      var data = {
        requestType: 'currencyReserveIncrease',
        account: $rootScope.selectedAccount,
        currency: currency.currency,
        amountPerUnitNQT: amountPerUnit
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.reserve.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.claimCurrency = function () {
      $scope.loading = true;

      var units = $filter('times')($scope.claim.units, Math.pow(10, currency.decimals));

      var data = {
        requestType: 'currencyReserveClaim',
        account: $rootScope.selectedAccount,
        currency: currency.currency,
        units: units
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.claim.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.buyCurrency = function () {
      $scope.loading = true;

      var quantityQNT = $filter('times')($scope.buy.quantity, Math.pow(10, currency.decimals));
      var priceNQT =  $filter('times')($scope.buy.price, 100000000);
      priceNQT = $filter('divide')(priceNQT,  Math.pow(10, currency.decimals));

      var data = {
        requestType: 'currencyBuy',
        account: $rootScope.selectedAccount,
        currency: currency.currency,
        rateNQT: priceNQT,
        units: quantityQNT
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.buy.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.sellCurrency = function () {
      $scope.loading = true;

      var quantityQNT = $filter('times')($scope.sell.quantity, Math.pow(10, currency.decimals));
      var priceNQT =  $filter('times')($scope.sell.price, 100000000);
      priceNQT = $filter('divide')(priceNQT,  Math.pow(10, currency.decimals));

      var data = {
        requestType: 'currencySell',
        account: $rootScope.selectedAccount,
        currency: currency.currency,
        rateNQT: priceNQT,
        units: quantityQNT
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.sell.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    $scope.fillBuyOffer = function (data) {
      $scope.buy.price = $filter('formatPriceNQT')(data.rateNQT, currency.decimals);
      $scope.buy.quantity = $filter('formatBalanceNQT')(data.supply, currency.decimals);
    };

    $scope.fillSellOffer = function (data) {
      $scope.sell.price = $filter('formatPriceNQT')(data.rateNQT, currency.decimals);
      $scope.sell.quantity = $filter('formatBalanceNQT')(data.supply, currency.decimals);
    };

    $scope.transferCurrency = function () {
      $scope.loading = true;

      var data = {
        requestType: 'transferCurrency',
        account: $rootScope.selectedAccount,
        currency: currency.currency,
        units: $scope.transfer.quantity * Math.pow(10, currency.decimals),
        quantity: $scope.transfer.quantity,
        recipient: $scope.transfer.recipient,
        deadline: 1440
      };

      MyNXT.sendTransaction(data, function (result) {
        $scope.loading = false;

        if(result && result.transaction) {
          $scope.transfer.transaction = result.transaction;
          $rootScope.refresh();
        }

        $scope.$apply();
      });
    };

    MyNXT.nrsRequest('getBuyOffers', { currency: currency.currency, availableOnly: true })
      .success(function (result) {
        if(result && result.offers && result.offers.length) {
          for(var i = 0; i < 3; i++) {
            var offer = result.offers[i];

            if(offer) {
              $scope.buy.offers.push(offer);
            }
          }

          $scope.sell.price = $filter('formatPriceNQT')($scope.buy.offers[0].rateNQT, currency.decimals);
        }
      });

    MyNXT.nrsRequest('getSellOffers', { currency: currency.currency, availableOnly: true })
      .success(function (result) {
        if(result && result.offers && result.offers.length) {
          for(var i = 0; i < 3; i++) {
            var offer = result.offers[i];

            if(offer) {
              $scope.sell.offers.push(offer);
            }
          }

          $scope.buy.price = $filter('formatPriceNQT')($scope.sell.offers[0].rateNQT, currency.decimals);
        }
      });

    $scope.$watch('selectedAccount', function () {
      $scope.currencyBalance = 0;
      MyNXT.nrsRequest('getAccountCurrencies', { account: $rootScope.selectedAccount })
        .success(function (result) {
          if(result && result.accountCurrencies) {
            var accountCurrencies = result.accountCurrencies;

            for(var i = 0; i < accountCurrencies.length; i++) {
              if(accountCurrencies[i].currency == currency.currency) {
                $scope.currencyBalance = $filter('formatBalanceNQT')(accountCurrencies[i].unconfirmedUnits, currency.decimals);
              }
            }
          }
        });
    });
  });