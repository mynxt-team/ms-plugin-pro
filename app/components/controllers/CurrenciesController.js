'use strict';

angular.module('ms-plugin')
  .controller('CurrenciesController', function ($scope, $rootScope, $compile, $modal, $filter, MyNXT, DTOptionsBuilder, DTColumnBuilder) {

    $scope.dtOptions = {
      dom: "<\"wrapper\"frtip>",
      lengthMenu: [],
      pageLength: 10,
      searching: true,
      processing: true,
      sort: true,
      order: [ 2, "desc"],
      language: {
        processing: '<img src="/img/loading_small.gif" />'
      },
      ajax: {
        url: "https://wallet.mynxt.info/nxt",
        dataSrc: "currencies",
        data: function (data) {
          var filter = {
            requestType: "getAllCurrencies"
          };
          return filter;
        }
      },
      fnCreatedRow: function(row, data, dataIndex) {
        $('button', row).unbind('click');
        $('button', row).bind('click', function() {
          $scope.$apply(function() {
            $scope.showModal(data.currency);
          });
        });

        $compile(angular.element(row).contents())($scope);
      }
    };

    $scope.dtColumns = [
      DTColumnBuilder.newColumn('code')
        .withTitle('Code')
        .renderWith(function(data, type, full) {
          return '<button class="btn-link">' + data + '</button>';
        }),
      DTColumnBuilder.newColumn('name')
        .withTitle('Name'),
      DTColumnBuilder.newColumn('numberOfExchanges')
        .withTitle('Exchanges'),
      DTColumnBuilder.newColumn('numberOfTransfers')
        .withTitle('Transfers'),
      DTColumnBuilder.newColumn('types')
        .withTitle('Type')
        .notSortable()
        .renderWith(function (data, type, full) {
          var html = '';
          for(var i = 0; i < data.length; i++) {
            html += '<type-icon type="' + data[i] +'"></type-icon> ';
          }

          return html;
        })
    ];

    $scope.showModal = function(currency) {
      MyNXT.nrsRequest('getCurrency', { currency: currency })
        .success(function (result) {
          if(result && !result.errorCode) {
            var scope =  $rootScope.$new();

            scope.currency = result;

            $modal({
              template: 'components/templates/currencyModal.html',
              show: true,
              persist: false, // destroy the new scope on hide
              backdrop: 'static',
              scope: scope
            });
          }
        });
    };
  });