'use strict';

angular.module('ms-plugin')
  .controller('AccountCurrenciesController', function ($scope, $rootScope, $routeParams, $modal, DTOptionsBuilder, MyNXT) {
    $scope.currencies = [];

    $scope.showModal = function(index) {
      MyNXT.nrsRequest('getCurrency', { code: $scope.currencies[index].code })
        .success(function (result) {
          if(result && !result.errorCode) {
            var scope =  $rootScope.$new();

            scope.currency = result;

            $modal({
              template: 'components/templates/currencyModal.html',
              show: true,
              persist: false, // destroy the new scope on hide
              backdrop: 'static',
              scope: scope
            });
          }
        });
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions().withDOM('<"wrapper"frtip>');

    $scope.$watch('selectedAccount', function () {
      $scope.currencies = [];
    });

    $scope.$watchCollection('[selectedAccount, random]', function () {
      MyNXT.nrsRequest('getAccountCurrencies', { account: $rootScope.selectedAccount })
        .success(function (result) {
          if(result && !result.errorCode && result.accountCurrencies) {
            $scope.currencies = result.accountCurrencies;
          }
        });
    });
  });
