'use strict';

angular.module('ms-plugin')
  .controller('MainController', function ($scope, $rootScope, $interval, MyNXT) {
    $rootScope.accounts = [];
    $rootScope.selectedAccount = '';
    $rootScope.random = Math.random();
    $rootScope.blockHeight = 0;

    $rootScope.changeAccount = function (account) {
      $rootScope.selectedAccount = account.tx_account_id;
    };

    $scope.refreshLoading = false;
    $rootScope.refresh = function () {
      $rootScope.random = Math.random();
      $scope.refreshLoading = true;
    };

    $rootScope.$on('cfpLoadingBar:completed', function () {
      $scope.refreshLoading = false;
    });

    $scope.$watchCollection('[selectedAccount, random]', function () {
      MyNXT.nrsRequest("getBlockchainStatus")
        .success(function (result) {
          if(result && result.numberOfBlocks) {
            $rootScope.blockHeight = result.numberOfBlocks;
          }
        });
    });

    MyNXT.getAccounts()
      .success(function (result) {
        if(result.status && result.status == "success" && result.data && result.data.accounts) {
          $rootScope.accounts = result.data.accounts;

          for(var i = 0; i < $rootScope.accounts.length; i++) {
            var account = $rootScope.accounts[i];
            if(account.bl_selected == 1) {
              $rootScope.selectedAccount = account.tx_account_id;
            }
          }
        }
      });

    $interval(function () {
      $rootScope.refresh();
    }, 60000)
  });