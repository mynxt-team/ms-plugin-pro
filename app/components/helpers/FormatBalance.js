angular.module('ms-plugin')
  .filter('formatBalanceNQT', function () {
    return function(amount, decimals) {
      if(!amount) amount = 0;
      if(!decimals) decimals = 0;

      var value = new BigNumber(amount);
      return value.dividedBy(Math.pow(10, decimals)).toNumber();
    };
  })
  .filter('formatPriceNQT', function () {
    return function (amount, decimals) {
      if(!amount) amount = 0;
      if(!decimals) decimals = 0;

      var value = new BigNumber(amount);
      return value.times(Math.pow(10, decimals)).dividedBy(Math.pow(10,8)).toNumber();
    };
  })
  .filter('decimal', function () {
    return function (text) {
      var parts = parseFloat(text).toFixed(8).split('.');
      parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      return parts.join('.');
    }
  });

