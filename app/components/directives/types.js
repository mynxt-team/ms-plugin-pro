angular.module('ms-plugin')
  .directive('typeIcon', ['$tooltip', '$compile', function ($tooltip, $compile) {
    return {
      replace: true,
      scope: { type: '=' },
      template: '<i class="fa"></i>',
      link: function(scope, element, attrs) {
        var icon = null;
        var title = null;

        var type = scope.type || attrs.type;

        switch(type) {
          case "EXCHANGEABLE":
            icon = "fa-exchange";
            title = "Exchangeable: This currency can be exchanged all within the NXT platform or on traditional exchanges";
            break;
          case "RESERVABLE":
            icon = "fa-university";
            title = "Reservable: These currency units are released and distributed if funding requirements are met within the given timescale, if not funds are automatically returned (e.g. crowdfunding)";
            break;
          case "CLAIMABLE":
            icon = "fa-archive";
            title = "Claimable: Reserved units can later be exchanged at an agreed rate";
            break;
          case "MINTABLE":
            icon = "fa-money";
            title = "Mintable: This currency can be mined by proof-of-work algorithms (SHA-256, SHA-3, Scrypt and keccak), whilst still being secured by NXT's proof-of-stake algorithm";
            break;
          case "CONTROLLABLE":
            icon = "fa-sliders";
            title = "Controllable: This currency may optionally only be traded with the issuing account (e.g. backed tokens such as gift vouchers)";
            break;
        }

        if(icon) {
          element.addClass(icon);
        }
        if (title) {
          $tooltip(element, { title: title });
        }
      }
    };
  }]);