'use strict';

angular.module('ms-plugin', [
  'ngRoute',
  'datatables',
  'mgcrea.ngStrap',
  'angular-ladda',
  'angular-loading-bar',
  'mynxt'
])
  .config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }])
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {

    })
  .otherwise({ redirectTo: '/' });
}]);